enum Direction
{
    up = 'up',
    left = 'left',
    down = 'down',
    right = 'right'
};

export default Direction;
