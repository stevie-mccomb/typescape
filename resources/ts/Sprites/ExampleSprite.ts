import SpriteConfig from 'Interfaces/SpriteConfig';

const config: SpriteConfig = {
    "src": "/img/sprites/example-sprite.png",

    "width": 32,
    "height": 32,

    "animations": {
        "idle": {
            "frames": [
                {
                    "x": 0,
                    "y": 0
                }
            ]
        }
    }
};

export default config;
